using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class TableView : MonoBehaviour
    {
        public event Action<int?> HoverWheelChangedEvent;
        
        [SerializeField] private Transform _lookPoint;
        [SerializeField] private List<WheelSlotView> _slots;

        [Inject] private GameModel _gameModel;

        public int? HoverWheel { get; private set; }
        public Transform LookPoint => _lookPoint;
        
        private bool _inputEnabled;
        private GameObject _activeCamera;

        private void Awake()
        {
            foreach (var slot in _slots)
            {
                slot.ClickedEvent += OnAnySlotClicked;
                slot.RightClickedEvent += OnAnySlotRightClicked;
                slot.HoverChangedEvent += OnAnySlotHoverChanged;
            }
        }

        private void OnEnable()
        {
            for (var i = 0; i < _slots.Count; i++)
            {
                _slots[i].UpdateModel(_gameModel.WheelModels[i]);
            }
            _gameModel.WheelModelChangedEvent += OnWheelModelChanged;
        }

        private void OnDisable()
        {
            _gameModel.WheelModelChangedEvent -= OnWheelModelChanged;
        }

        private void OnWheelModelChanged(int index)
        {
            _slots[index].UpdateModel(_gameModel.WheelModels[index]);
        }

        public void SetInputEnabled(bool value)
        {
            _inputEnabled = value;
            if (!_inputEnabled)
            {
                OnBackPressed();
            }
            foreach (var slot in _slots)
            {
                slot.SetInputEnabled(value);
            }
        }

        public bool OnBackPressed()
        {
            if (_activeCamera != null)
            {
                _activeCamera.SetActive(false);
                _activeCamera = null;
                return true;
            }
            return false;
        }
        
        private void OnAnySlotClicked(WheelSlotView slotView)
        {
            if (!_inputEnabled || _activeCamera != null)
                return;

            if (_gameModel.NewWheel != null)
            {
                _gameModel.ApplyNewWheel(_slots.IndexOf(slotView));
            }
            else if (_gameModel.NewHamster != null)
            {
                _gameModel.SwapNewHamster(_slots.IndexOf(slotView));
            }
            else if (slotView.WheelView != null)
            {
                _gameModel.MoveHamster(_slots.IndexOf(slotView));
            }
        }
        
        private void OnAnySlotRightClicked(WheelSlotView slotView)
        {
            if (!_inputEnabled || _activeCamera != null)
                return;
            
            if (_gameModel.NewWheel == null && _gameModel.NewHamster == null && slotView.WheelView != null)
            {
                _activeCamera = slotView.WheelView.VirtualCamera;
                _activeCamera.SetActive(true);
            }
        }
        
        private void OnAnySlotHoverChanged(WheelSlotView slotView, bool isHovered)
        {
            var oldHoverWheel = HoverWheel;
            
            if (isHovered)
                HoverWheel = _slots.IndexOf(slotView);

            if (HoverWheel != null && !isHovered && _slots[HoverWheel.Value] == slotView)
                HoverWheel = null;
            
            if (oldHoverWheel != HoverWheel)
                HoverWheelChangedEvent?.Invoke(HoverWheel);
        }
    }
}