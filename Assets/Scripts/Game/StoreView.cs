using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class StoreView : MonoBehaviour
    {
        public event Action BackClickedAction;
        
        [SerializeField] public GameObject _itemPrefab;
        [SerializeField] public TextMeshProUGUI _titleText;
        [SerializeField] public TextMeshProUGUI _balanceText;
        [SerializeField] public Transform _itemsContainer;
        [SerializeField] public Button _backButton;
        [SerializeField] public Button _previousButton;
        [SerializeField] public Button _nextButton;
        [SerializeField] public TextMeshProUGUI _pageText;

        [Inject] private IInstantiator _instantiator;
        [Inject] private StoreModel _storeModel;
        [Inject] private GameModel _gameModel;

        private GameObjectPool<StoreItemView> _itemsPool;
        private List<StoreItemView> _itemViews = new List<StoreItemView>();
        private int _currentPage;

        private void Awake()
        {
            _itemsPool = new GameObjectPool<StoreItemView>(_itemPrefab, _itemsContainer, _instantiator);

            _backButton.onClick.AddListener(OnBackButtonClicked);
            _previousButton.onClick.AddListener(OnPreviousButtonClicked);
            _nextButton.onClick.AddListener(OnNextButtonClicked);
        }

        private void OnEnable()
        {
            SetPage(0);
            UpdateBalance();

            _gameModel.BalanceChangedEvent += OnBalanceChanged;
            _storeModel.PageChangedEvent += OnPageChanged;
        }

        private void OnDisable()
        {
            _gameModel.BalanceChangedEvent -= OnBalanceChanged;
            _storeModel.PageChangedEvent -= OnPageChanged;
        }
        
        private void OnBalanceChanged(int balance)
        {
            UpdateBalance();
        }

        private void OnPageChanged(StorePageModel page)
        {
            if (page == _storeModel.Pages[_currentPage])
                SetPage(_currentPage);
        }

        private void SetPage(int index)
        {
            _currentPage = index;
            var pageModel = _storeModel.Pages[index];

            _titleText.text = pageModel.Name;
            
            PoolUtils.SetItemsCount(_itemViews, _itemsPool, pageModel.Items.Count);
            for (var i = 0; i < _itemViews.Count; i++)
            {
                _itemViews[i].transform.SetSiblingIndex(i);
                _itemViews[i].SetItem(pageModel.Items[i]);
            }

            _pageText.text = $"{_currentPage + 1}/{_storeModel.Pages.Count}";
        }
        
        private void UpdateBalance()
        {
            _balanceText.text = _gameModel.Balance.ToString();
        }
        
        private void OnBackButtonClicked()
        {
            BackClickedAction?.Invoke();
        }

        private void OnPreviousButtonClicked()
        {
            if (_currentPage > 0)
                SetPage(_currentPage - 1);   
        }

        private void OnNextButtonClicked()
        {
            if (_currentPage < _storeModel.Pages.Count - 1)
                SetPage(_currentPage + 1);
        }
    }
}