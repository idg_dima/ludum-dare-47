using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class StoreItemView : MonoBehaviour
    {
        [SerializeField] private Color _priceColor;
        [SerializeField] private Color _noMoneyColor;
        [SerializeField] private Button _button;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private TextMeshProUGUI _descriptionText;
        [SerializeField] private TextMeshProUGUI _priceText;
        [SerializeField] private GameObject _priceContainer;
        [SerializeField] private GameObject _soldOutContainer;

        [Inject] private GameModel _gameModel;
        
        private StoreItemModel _model;
        private bool _canBuy;

        public void SetItem(StoreItemModel itemModel)
        {
            if (_model != null)
                _model.PurchasedEvent += OnItemPurchased;

            _model = itemModel;
            UpdateView();

            _model.PurchasedEvent += OnItemPurchased;
        }

        private void Awake()
        {
            _button.onClick.AddListener(OnButtonClicked);
        }

        private void OnEnable()
        {
            if (_model != null)
                _model.PurchasedEvent += OnItemPurchased;

            _gameModel.BalanceChangedEvent += OnBalanceChanged;
        }

        private void OnDisable()
        {
            if (_model != null)
                _model.PurchasedEvent -= OnItemPurchased;
            
            _gameModel.BalanceChangedEvent -= OnBalanceChanged;
        }

        private void OnItemPurchased()
        {
            UpdateView();
        }
        
        private void OnButtonClicked()
        {
            _gameModel.TryPurchase(_model);
        }

        private void UpdateView()
        {
            _canvasGroup.alpha = _model.IsSoldOut ? 0.75f : 1f;
            _titleText.text = _model.Name;
            _descriptionText.text = _model.Description;
            UpdatePrice();
        }
        
        private void OnBalanceChanged(int balance)
        {
            if (_model != null && !_model.IsSoldOut && !_canBuy && _model.Price <= _gameModel.Balance)
                UpdatePrice();
        }

        private void UpdatePrice()
        {
            _priceContainer.SetActive(!_model.IsSoldOut);
            _soldOutContainer.SetActive(_model.IsSoldOut);

            if (!_model.IsSoldOut)
            {
                _canBuy = _gameModel.Balance >= _model.Price;
                _priceText.text = _model.Price.ToString();
                _priceText.color = _canBuy ? _priceColor : _noMoneyColor;
            }
        } 
    }
}