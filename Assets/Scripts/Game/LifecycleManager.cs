using System;
using UnityEngine;

namespace Game
{
    public class LifecycleManager : MonoBehaviour
    {
        public event Action UpdateEvent;

        private void Update()
        {
            UpdateEvent?.Invoke();
        }
    }
}