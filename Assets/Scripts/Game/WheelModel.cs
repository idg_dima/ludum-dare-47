using System;
using System.Collections.Generic;
using Game.Dic;
using UnityEngine;

namespace Game
{
    public class WheelModel
    {
        public event Action<HamsterModel, string> HamsterLevelUpEvent;
        
        public WheelType WheelType { get; private set; }
        public HamsterModel HamsterModel;

        public int Energy => WheelType == WheelType.None || HamsterModel == null ? 0 : HamsterModel.Energy * _wheelInfo.EnergyMultiplier / 100;

        private readonly GameDictionary _dictionary;
        private WheelInfo _wheelInfo;
        
        public WheelModel(GameDictionary dictionary, WheelType wheelType)
        {
            _dictionary = dictionary;
            SetWheelType(wheelType);
        }

        public void SetHamsterModel(HamsterModel hamsterModel)
        {
            if (HamsterModel != null)
                HamsterModel.LevelUpEvent -= OnHamsterLevelUp;

            HamsterModel = hamsterModel;
            if (HamsterModel != null)
                HamsterModel.LevelUpEvent += OnHamsterLevelUp;
        }

        private void OnHamsterLevelUp(HamsterModel hamsterModel, string message)
        {
            HamsterLevelUpEvent(hamsterModel, message);
        }

        public void SetWheelType(WheelType type)
        {
            WheelType = type;

            if (type == WheelType.None)
                _wheelInfo = null;
            else
                _wheelInfo = _dictionary.GetWheelInfo(type);
        }

        public void Update(List<WheelModel> allWheels)
        {
            if (HamsterModel != null)
            {
                HamsterModel.DoWork(Time.deltaTime, allWheels);
            }
        }
    }
}