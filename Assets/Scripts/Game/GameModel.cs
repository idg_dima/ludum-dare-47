using System;
using System.Collections.Generic;
using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class GameModel : IDisposable
    {
        public event Action<string> HamsterLevelUpEvent; 
        
        public event Action<int> BalanceChangedEvent;
        public event Action<WheelTableType> WheelTableChangedEvent; 
        public event Action<int> WheelModelChangedEvent;
        public event Action<PcType> PcChangedEvent; 
        public event Action<int> GpuModelChangedEvent;

        public event Action NewGpuChangedEvent;
        public event Action NewWheelChangedEvent;
        public event Action NewHamsterChangedEvent;

        public int Balance { get; private set; }
        public float CoinProgress { get; private set; }
        public WheelTableType WheelTable { get; private set; }
        public List<WheelModel> WheelModels { get; } = new List<WheelModel>();
        public PcType Pc { get; private set; }
        public List<GpuModel> GpuModels { get; } = new List<GpuModel>();

        private GameDictionary _gameDictionary;
        private LifecycleManager _lifecycleManager;

        public GpuType? NewGpu { get; private set; }
        public WheelType? NewWheel { get; private set; }
        public HamsterModel NewHamster { get; private set; }

        [Inject]
        public GameModel(GameDictionary gameDictionary, LifecycleManager lifecycleManager)
        {
            _gameDictionary = gameDictionary;
            _lifecycleManager = lifecycleManager;
            _lifecycleManager.UpdateEvent += OnUpdate;

            SetTableType(WheelTableType.Tier1);
            SetWheelType(0, WheelType.Tier1);
            SetHamster(0, new HamsterModel(true));

            SetPcType(PcType.Tier1);
            SetGpuType(0, GpuType.Tier1);
        }

        private void OnUpdate()
        {
            CoinProgress += GetCurrentProductionRate() * Time.deltaTime;
            if (CoinProgress > 1)
            {
                Balance += (int) CoinProgress;
                CoinProgress %= 1;
                BalanceChangedEvent?.Invoke(Balance);
            }

            foreach (var wheelModel in WheelModels)
            {
                wheelModel.Update(WheelModels);
            }
        }

        public void SetTableType(WheelTableType tableType)
        {
            WheelTable = tableType;
            var tableInfo = _gameDictionary.GetTableInfo(tableType);
            while (WheelModels.Count < tableInfo.WheelSlots)
            {
                var wheelModel = new WheelModel(_gameDictionary, WheelType.None);
                wheelModel.HamsterLevelUpEvent += OnAnyHamsterLevelUp;
                WheelModels.Add(wheelModel);
            }
            while (WheelModels.Count > tableInfo.WheelSlots)
            {
                WheelModels.RemoveAt(WheelModels.Count - 1);
            }
            WheelTableChangedEvent?.Invoke(tableType);
        }

        private void OnAnyHamsterLevelUp(HamsterModel model, string message)
        {
            HamsterLevelUpEvent?.Invoke(message);
        }

        private void SetWheelType(int index, WheelType type)
        {
            var model = WheelModels[index];
            model.SetWheelType(type);
            WheelModelChangedEvent?.Invoke(index);
        }

        private void SetHamster(int index, HamsterModel hamsterModel)
        {
            var wheelModel = WheelModels[index];
            if (wheelModel.WheelType == WheelType.None)
                return;
            
            wheelModel.SetHamsterModel(hamsterModel);
            WheelModelChangedEvent?.Invoke(index);
        }

        private void SwapHamsters(int index1, int index2)
        {
            if (index1 == index2)
                return;

            var tmp = WheelModels[index1].HamsterModel;
            WheelModels[index1].SetHamsterModel(WheelModels[index2].HamsterModel);
            WheelModels[index2].SetHamsterModel(tmp);
            
            WheelModelChangedEvent?.Invoke(index1);
            WheelModelChangedEvent?.Invoke(index2);
        }

        public void SetPcType(PcType pcType)
        {
            Pc = pcType;
            var pcInfo = _gameDictionary.GetPcInfo(pcType);
            while (GpuModels.Count < pcInfo.GpuSlots)
            {
                GpuModels.Add(new GpuModel(_gameDictionary, GpuType.None));
            }
            while (GpuModels.Count > pcInfo.GpuSlots)
            {
                GpuModels.RemoveAt(GpuModels.Count - 1);
            }
            PcChangedEvent?.Invoke(pcType);
        }

        private void SetGpuType(int index, GpuType type)
        {
            var model = GpuModels[index];
            model.SetGpuType(type);
            GpuModelChangedEvent?.Invoke(index);
        }

        public void MoveGpu(int index)
        {
            if (GpuModels[index].GpuType != GpuType.None)
            {
                NewGpu = GpuModels[index].GpuType;
                GpuModels[index].SetGpuType(GpuType.None);
                NewGpuChangedEvent?.Invoke();
                GpuModelChangedEvent?.Invoke(index);
            }
        }
        
        public void MoveHamster(int index)
        {
            var wheelModel = WheelModels[index];
            if (wheelModel.WheelType != WheelType.None && wheelModel.HamsterModel != null)
            {
                NewHamster = wheelModel.HamsterModel;
                wheelModel.SetHamsterModel(null);
                NewHamsterChangedEvent?.Invoke();
                WheelModelChangedEvent?.Invoke(index);
            }
        }

        public int GetCurrentEnergy()
        {
            var result = 0;
            foreach (var wheelModel in WheelModels)
            {
                var hamstersMultipler = 0;
                foreach (var wheelModel2 in WheelModels)
                {
                    if (wheelModel.HamsterModel != null && wheelModel2.HamsterModel != null && wheelModel2.HamsterModel != wheelModel.HamsterModel)
                        hamstersMultipler += wheelModel.HamsterModel.OtherHamstersMultiplier;
                }

                result += Mathf.RoundToInt(wheelModel.Energy * (1 + hamstersMultipler / 100f));
            }
            return result;
        }

        public float GetCurrentProductionRate()
        {
            var hamstersMultipler = 0;
            foreach (var wheelModel in WheelModels)
            {
                if (wheelModel.HamsterModel != null)
                    hamstersMultipler += wheelModel.HamsterModel.CoinsMultiplier;
            }
            
            var result = 0f;
            var energyLeft = GetCurrentEnergy();
            foreach (var gpuModel in GpuModels)
            {
                gpuModel.IsActive = energyLeft >= gpuModel.EnergyRequirement;
                if (gpuModel.IsActive)
                {
                    result += gpuModel.ProductionRate * (1 + hamstersMultipler / 100f);
                    energyLeft -= gpuModel.EnergyRequirement;
                }
                else
                {
                    return result;
                }
            }
            return result;
        }

        public void TryPurchase(StoreItemModel itemModel)
        {
            if (Balance < itemModel.Price)
                return;
            
            Balance -= itemModel.Price;
            BalanceChangedEvent?.Invoke(Balance);
            itemModel.Purchase();
        }

        public void GiveWheel(WheelType wheelType)
        {
            NewWheel = wheelType;
            NewWheelChangedEvent?.Invoke();
        }
        
        public void GiveGpu(GpuType gpuType)
        {
            NewGpu = gpuType;
            NewGpuChangedEvent?.Invoke();
        }

        public void GiveHamster()
        {
            NewHamster = new HamsterModel();
            NewHamsterChangedEvent?.Invoke();
        }

        public void ApplyNewWheel(int index)
        {
            SetWheelType(index, NewWheel.Value);
            NewWheel = null;
            NewWheelChangedEvent?.Invoke();
        }
        
        public void ApplyNewGpu(int index)
        {
            SetGpuType(index, NewGpu.Value);
            NewGpu = null;
            NewGpuChangedEvent?.Invoke();
        }
        
        public void SwapNewGpu(int index)
        {
            var tmp = GpuModels[index].GpuType;
            SetGpuType(index, NewGpu.Value);
            if (tmp == GpuType.None)
                NewGpu = null;
            else
                NewGpu = tmp;
            NewGpuChangedEvent?.Invoke();
        }

        public void DeleteNewGpu()
        {
            NewGpu = null;
            NewGpuChangedEvent?.Invoke();
        }

        public void SwapNewHamster(int index)
        {
            var wheelModel = WheelModels[index];
            if (wheelModel.WheelType == WheelType.None)
                return;

            var tmp = wheelModel.HamsterModel;
            SetHamster(index, NewHamster);
            NewHamster = tmp;
            NewHamsterChangedEvent?.Invoke();
        }

        public void DeleteNewHamster()
        {
            NewHamster = null;
            NewHamsterChangedEvent?.Invoke();
        }

        public void Dispose()
        {
            _lifecycleManager.UpdateEvent -= OnUpdate;
        }
    }
}