using System;
using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class WheelSlotView : MonoBehaviour
    {
        public Action<WheelSlotView> ClickedEvent;
        public Action<WheelSlotView> RightClickedEvent;
        public Action<WheelSlotView, bool> HoverChangedEvent;
        
        [SerializeField] private ClickableCollider _collider;
        [SerializeField] private Transform _wheelContainer;
        [Inject] private IInstantiator _instantiator;
        [Inject] private GameDictionary _dictionary;

        public WheelView WheelView { get; private set; }
        private WheelType _wheelType;
        private GameObject _wheel;
     
        private void Awake()
        {
            _collider.ClickedEvent += () => ClickedEvent?.Invoke(this);
            _collider.RightClickedEvent += () => RightClickedEvent?.Invoke(this);
            _collider.HoveredChangedEvent += x => HoverChangedEvent?.Invoke(this, x);
        }
        
        public void UpdateModel(WheelModel model)
        {
            if (_wheelType != model.WheelType)
                SetWheelType(model.WheelType);

            if (_wheelType != WheelType.None)
                WheelView.UpdateModel(model);
        }

        private void SetWheelType(WheelType wheelType)
        {
            if (WheelView != null)
            {
                Destroy(WheelView.gameObject);
                WheelView = null;
            }

            _wheelType = wheelType;
            if (_wheelType != WheelType.None)
            {
                var wheelInfo = _dictionary.GetWheelInfo(_wheelType);
                var wheelObject = _instantiator.InstantiatePrefab(wheelInfo.Prefab, _wheelContainer);
                WheelView = wheelObject.GetComponent<WheelView>();
            }
        }

        public void SetInputEnabled(bool value)
        {
            _collider.gameObject.SetActive(value);
        }
    }
}