using Game.Dic;

namespace Game
{
    public class GpuModel
    {
        public GpuType GpuType { get; private set; }
        
        public bool IsActive { get; set; }
        
        public int EnergyRequirement => GpuType == GpuType.None ? 0 : _gpuInfo.EnergyRequirement;
        public float ProductionRate => GpuType == GpuType.None ? 0 : _gpuInfo.ProductionRate;
        
        private readonly GameDictionary _dictionary;
        private GpuInfo _gpuInfo;

        public GpuModel(GameDictionary dictionary, GpuType gpuType)
        {
            _dictionary = dictionary;
            SetGpuType(gpuType);
        }
        
        public void SetGpuType(GpuType type)
        {
            GpuType = type;

            if (type == GpuType.None)
                _gpuInfo = null;
            else
                _gpuInfo = _dictionary.GetGpuInfo(type);
        }
    }
}