using UnityEngine;

namespace Game
{
    public class HamsterView : MonoBehaviour
    {
        private static readonly int VelocityParameter = Animator.StringToHash("Velocity");
        
        [SerializeField] private Animator _animator;

        private HamsterModel _model;
        
        public void SetModel(HamsterModel hamsterModel)
        {
            _model = hamsterModel;
            
            _animator.SetFloat(VelocityParameter, _model.Energy);
        }
    }
}