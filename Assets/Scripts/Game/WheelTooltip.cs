using Game.Dic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Game
{
    public class WheelTooltip : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _descriptionText;

        [Inject] private GameDictionary _dictionary;
        
        public void SetWheelType(WheelType wheelType)
        {
            var wheelInfo = _dictionary.GetWheelInfo(wheelType);
            _nameText.text = wheelInfo.Name;
            _descriptionText.text = wheelInfo.Description;
        }

        public void SetEmpty()
        {
            _nameText.text = "Empty slot";
            _descriptionText.text = "Purchase wheel in store and place it here";
        }
    }
}