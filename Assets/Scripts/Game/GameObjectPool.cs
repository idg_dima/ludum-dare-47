using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class GameObjectPool<T> where T : Component
    {
        private readonly GameObject _prefab;
        private readonly IInstantiator _instantiator;
        private readonly Transform _parent;
        private readonly Queue<T> _freeItems = new Queue<T>();

        public GameObjectPool(GameObject prefab, Transform parent, IInstantiator instantiator)
        {
            _prefab = prefab;
            _parent = parent;
            _instantiator = instantiator;
        }

        public T Obtain()
        {
            if (_freeItems.Count > 0)
            {
                var item = _freeItems.Dequeue();
                item.gameObject.SetActive(true);
                return item;
            }

            var newGameObject = _instantiator.InstantiatePrefab(_prefab, _parent);
            return newGameObject.GetComponent<T>();
        }

        public void Free(T item)
        {
            item.gameObject.SetActive(false);
            _freeItems.Enqueue(item);

            if (item.transform.parent != _parent)
                item.transform.SetParent(_parent);
        }
    }
    
    public class GameObjectPool
    {
        private readonly GameObject _prefab;
        private readonly IInstantiator _instantiator;
        private readonly Transform _parent;
        private readonly Queue<GameObject> _freeItems = new Queue<GameObject>();

        public GameObjectPool(GameObject prefab, Transform parent, IInstantiator instantiator)
        {
            _prefab = prefab;
            _parent = parent;
            _instantiator = instantiator;
        }

        public GameObject Obtain()
        {
            if (_freeItems.Count > 0)
            {
                var item = _freeItems.Dequeue();
                item.SetActive(true);
                return item;
            }

            var newGameObject = _instantiator.InstantiatePrefab(_prefab, _parent);
            return newGameObject;
        }

        public void Free(GameObject item)
        {
            item.SetActive(false);
            _freeItems.Enqueue(item);

            if (item.transform.parent != _parent)
                item.transform.SetParent(_parent);
        }
    }
}