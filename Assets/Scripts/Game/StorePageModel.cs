using System.Collections.Generic;

namespace Game
{
    public class StorePageModel
    {
        public string Name;
        public List<StoreItemModel> Items;

        public StorePageModel(string name)
        {
            Name = name;
        }
    }
}