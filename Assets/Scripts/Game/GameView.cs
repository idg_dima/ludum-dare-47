using System;
using Cinemachine;
using Generated;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class GameView : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _mainCamera;
        [SerializeField] private CinemachineVirtualCamera _screenCamera;
        [SerializeField] private CinemachineVirtualCamera _pcCamera;
        [SerializeField] private CinemachineVirtualCamera _hamstersCamera;
        [SerializeField] private ClickableCollider _screenCollider;
        [SerializeField] private ClickableCollider _pcCollider;
        [SerializeField] private ClickableCollider _hamstersCollider;
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private ComputerScreenView _computerScreenView;
        [SerializeField] private WheelsTableView _wheelsTable;
        [SerializeField] private PcContainerView _pcContainerView;
        [SerializeField] private WheelsUi wheelsUi;
        [SerializeField] private GpusUi gpusUi;
        [SerializeField] private GameObject _levelUpContainer;
        [SerializeField] private TextMeshProUGUI _levelUpText;
        [SerializeField] private Button _levelUpButton;

        [Inject] private InputActions _inputActions;
        [Inject] private GameModel _gameModel;
        
        private CameraState _cameraState;
        private bool _backEnabled = true;
    
        private void Awake()
        {
            _screenCollider.ClickedEvent += OnScreenClicked;
            _pcCollider.ClickedEvent += OnPcClicked;
            _hamstersCollider.ClickedEvent += OnHamstersClicked;
            _backButton.onClick.AddListener(OnBackButtonClicked);
            _exitButton.onClick.AddListener(OnExitButtonClicked);
            _levelUpButton.onClick.AddListener(OnLevelUpButtonClicked);

            SetCameraState(CameraState.Main);
        }

        private void OnEnable()
        {
            _inputActions.Default.Back.performed += OnBackPerformed;

            _gameModel.NewGpuChangedEvent += OnNewGpuChanged;
            _gameModel.NewWheelChangedEvent += OnNewWheelChanged;
            _gameModel.NewHamsterChangedEvent += OnNewHamsterChanged;
            _gameModel.HamsterLevelUpEvent += OnHamsterLevelUp;
        }

        private void OnHamsterLevelUp(string message)
        {
            _levelUpContainer.SetActive(true);
            _levelUpText.text = message;
        }

        private void OnLevelUpButtonClicked()
        {
            _levelUpContainer.SetActive(false);
        }

        private void OnDisable()
        {
            _inputActions.Default.Back.performed -= OnBackPerformed;
            
            _gameModel.NewGpuChangedEvent -= OnNewGpuChanged;
            _gameModel.NewWheelChangedEvent -= OnNewWheelChanged;
            _gameModel.NewHamsterChangedEvent -= OnNewHamsterChanged;
            _gameModel.HamsterLevelUpEvent -= OnHamsterLevelUp;
        }

        private void OnScreenClicked()
        {
            if (_cameraState == CameraState.Main)
                SetCameraState(CameraState.Screen);
        }

        private void OnPcClicked()
        {
            if (_cameraState == CameraState.Main)
                SetCameraState(CameraState.Pc);
        }

        private void OnHamstersClicked()
        {
            if (_cameraState == CameraState.Main)
                SetCameraState(CameraState.Hamsters);
        }
        
        private void OnBackButtonClicked()
        {
            switch (_cameraState)
            {
                case CameraState.Main:
                    break;
                case CameraState.Screen:
                case CameraState.Pc:
                    SetCameraState(CameraState.Main);
                    break;
                case CameraState.Hamsters:
                    if (!_wheelsTable.OnBackPressed())
                        SetCameraState(CameraState.Main);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
        
        private void OnBackPerformed(InputAction.CallbackContext context)
        {
            if (_backEnabled && _cameraState != CameraState.Main)
                SetCameraState(CameraState.Main);
        }

        private void SetCameraState(CameraState state)
        {
            _cameraState = state;

            if (_cameraState == CameraState.Hamsters)
                _hamstersCamera.LookAt = _wheelsTable.LookPoint;

            if (_cameraState == CameraState.Pc)
                _pcCamera.LookAt = _pcContainerView.LookPoint;
            
            SetActiveCamera(GetCameraForState(state));
            _backButton.gameObject.SetActive(_backButton && _cameraState != CameraState.Main);
            
            _computerScreenView.SetInputEnabled(state == CameraState.Screen);
            _wheelsTable.SetInputEnabled(state == CameraState.Hamsters);
            _pcContainerView.SetInputEnabled(state == CameraState.Pc);
            
            gpusUi.gameObject.SetActive(state == CameraState.Pc);
            wheelsUi.gameObject.SetActive(state == CameraState.Hamsters);
        }

        private CinemachineVirtualCamera GetCameraForState(CameraState state)
        {
            switch (state)
            {
                case CameraState.Main:
                    return _mainCamera;
                case CameraState.Screen:
                    return _screenCamera;
                case CameraState.Pc:
                    return _pcCamera;
                case CameraState.Hamsters:
                    return _hamstersCamera;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void SetActiveCamera(CinemachineVirtualCamera virtualCamera)
        {
            _mainCamera.gameObject.SetActive(false);
            _screenCamera.gameObject.SetActive(false);
            _pcCamera.gameObject.SetActive(false);
            _hamstersCamera.gameObject.SetActive(false);
            
            virtualCamera.gameObject.SetActive(true);
        }

        private void SetBackEnabled(bool value)
        {
            _backEnabled = value;
            _backButton.gameObject.SetActive(_backEnabled && _cameraState != CameraState.Main);
        }
        
        private void OnNewGpuChanged()
        {
            if (_gameModel.NewGpu != null)
                SetCameraState(CameraState.Pc);
            
            UpdateBackEnabled();
        }

        private void OnNewWheelChanged()
        {
            if (_gameModel.NewWheel != null)
                SetCameraState(CameraState.Hamsters);
            
            UpdateBackEnabled();
        }

        private void OnNewHamsterChanged()
        {
            if (_gameModel.NewHamster != null)
                SetCameraState(CameraState.Hamsters);
            
            UpdateBackEnabled();
        }

        private void UpdateBackEnabled()
        {
            SetBackEnabled(_gameModel.NewHamster == null && _gameModel.NewGpu == null && _gameModel.NewWheel == null);
        }
    }
}