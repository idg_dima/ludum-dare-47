using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public static class PoolUtils
    {
        public static void SetItemsCount(List<GameObject> items, GameObjectPool pool, int count)
        {
            for (var i = items.Count - 1; i >= count; i--)
            {
                pool.Free(items[i]);
                items.RemoveAt(i);
            }

            for (var i = items.Count; i < count; i++)
            {
                items.Add(pool.Obtain());
            }
        }
        
        public static void SetItemsCount<T>(List<T> items, GameObjectPool<T> pool, int count) where T : Component
        {
            for (var i = items.Count - 1; i >= count; i--)
            {
                pool.Free(items[i]);
                items.RemoveAt(i);
            }

            for (var i = items.Count; i < count; i++)
            {
                items.Add(pool.Obtain());
            }
        }
    }
}