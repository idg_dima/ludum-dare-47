using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class ClickableCollider : MonoBehaviour
    {
        public event Action ClickedEvent;
        public event Action RightClickedEvent;
        public event Action<bool> HoveredChangedEvent;

        [SerializeField] private bool AllowClickingThroughUi;
        
        public bool IsHovered { get; private set; }

        public void Click()
        {
            if (AllowClickingThroughUi || !EventSystem.current.IsPointerOverGameObject())
                ClickedEvent?.Invoke();
        }
        
        public void RightClick()
        {
            if (AllowClickingThroughUi || !EventSystem.current.IsPointerOverGameObject())
                RightClickedEvent?.Invoke();
        }

        public void SetIsHovered(bool value)
        {
            if (IsHovered == value)
                return;

            IsHovered = value;
            HoveredChangedEvent?.Invoke(value);
        }
    }
}