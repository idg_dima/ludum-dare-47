using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class WheelView : MonoBehaviour
    {
        [SerializeField] private Transform _hamsterContainer;
        [SerializeField] private Transform _rotatingPart;
        [SerializeField] private Vector3 _rotationAxis;
        private float _rotationSpeed;
        [SerializeField] private GameObject _virtualCamera;

        [Inject] private IInstantiator _instantiator;
        [Inject] private GameDictionary _dictionary;

        public GameObject VirtualCamera => _virtualCamera;
        
        private WheelModel _model;
        private HamsterView _hamsterView;

        public void UpdateModel(WheelModel model)
        {
            _model = model;
            UpdateHamster();
            UpdateSpeed();
        }

        private void Update()
        {
            if (_model.HamsterModel != null)
                _rotatingPart.RotateAround(_rotatingPart.transform.position, _rotationAxis, Time.deltaTime * _rotationSpeed);
        }

        private void UpdateHamster()
        {
            if (_hamsterView == null && _model.HamsterModel != null)
            {
                var hamsterObject = _instantiator.InstantiatePrefab(_dictionary.HamsterPrefab, _hamsterContainer);
                _hamsterView = hamsterObject.GetComponent<HamsterView>();
                _hamsterView.SetModel(_model.HamsterModel);
            }

            if (_hamsterView != null && _model.HamsterModel == null)
            {
                Destroy(_hamsterView.gameObject);
                _hamsterView = null;
            }
        }

        private void UpdateSpeed()
        {
            _rotationSpeed = _model.HamsterModel == null ? 0 : -_model.HamsterModel.Energy;
        }
    }
}