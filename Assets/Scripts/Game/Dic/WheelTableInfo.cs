using System;
using UnityEngine;

namespace Game.Dic
{
    [Serializable]
    public class WheelTableInfo
    {
        public WheelTableType Type;
        public GameObject Prefab;
        public int WheelSlots;
    }
}