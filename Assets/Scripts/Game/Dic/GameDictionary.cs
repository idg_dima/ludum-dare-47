using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Dic
{
    [CreateAssetMenu(menuName = "Game/GameDictionary")]
    public class GameDictionary : ScriptableObject
    {
        public GameObject HamsterPrefab;
        public List<WheelTableInfo> WheelTables;
        public List<WheelInfo> Wheels;
        public List<PcInfo> Pcs;
        public List<GpuInfo> Gpus;

        public WheelTableInfo GetTableInfo(WheelTableType tableType)
        {
            return WheelTables.First(x => x.Type == tableType);
        }

        public WheelInfo GetWheelInfo(WheelType wheelType)
        {
            return Wheels.First(x => x.Type == wheelType);
        }
        
        public GpuInfo GetGpuInfo(GpuType gpuType)
        {
            return Gpus.First(x => x.Type == gpuType);
        }

        public PcInfo GetPcInfo(PcType pcType)
        {
            return Pcs.First(x => x.Type == pcType);
        }
    }
}