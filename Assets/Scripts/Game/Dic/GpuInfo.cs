using System;
using UnityEngine;

namespace Game.Dic
{
    [Serializable]
    public class GpuInfo
    {
        public GpuType Type;
        public GameObject Prefab;
        public int EnergyRequirement;
        public float ProductionRate;
        
        public string Name;
        [Multiline] public string Description;
    }
}