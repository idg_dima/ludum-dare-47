using System;
using UnityEngine;

namespace Game.Dic
{
    [Serializable]
    public class WheelInfo
    {
        public WheelType Type;
        public GameObject Prefab;
        public int EnergyMultiplier;
        public string Name;
        [Multiline] public string Description;
    }
}