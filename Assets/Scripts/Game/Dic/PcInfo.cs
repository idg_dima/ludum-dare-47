using System;
using UnityEngine;

namespace Game.Dic
{
    [Serializable]
    public class PcInfo
    {
        public PcType Type;
        public GameObject Prefab;
        public int GpuSlots;
    }
}