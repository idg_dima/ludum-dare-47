using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class PcView : MonoBehaviour
    {
        public event Action<int?> HoverGpuChangedEvent;
        
        [SerializeField] private Transform _lookPoint;
        [SerializeField] private List<GpuSlotView> _slots;

        [Inject] private GameModel _gameModel;
        
        public int? HoverGpu { get; private set; }
        public Transform LookPoint => _lookPoint;
        
        private bool _inputEnabled;

        private void Awake()
        {
            foreach (var slot in _slots)
            {
                slot.ClickedEvent += OnAnySlotClicked;
                slot.HoverChangedEvent += OnAnySlotHoverChanged;
            }
        }

        private void OnEnable()
        {
            for (var i = 0; i < _slots.Count; i++)
            {
                _slots[i].UpdateModel(_gameModel.GpuModels[i]);
            }
            _gameModel.GpuModelChangedEvent += OnGpuModelChanged;
        }

        private void OnDisable()
        {
            _gameModel.GpuModelChangedEvent -= OnGpuModelChanged;
        }

        private void OnGpuModelChanged(int index)
        {
            _slots[index].UpdateModel(_gameModel.GpuModels[index]);
        }

        public void SetInputEnabled(bool value)
        {
            _inputEnabled = value;
            foreach (var slot in _slots)
            {
                slot.SetInputEnabled(value);
            }
        }
        
        
        private void OnAnySlotClicked(GpuSlotView slotView)
        {
            if (_gameModel.NewGpu != null)
            {
                var index = _slots.IndexOf(slotView);
                _gameModel.SwapNewGpu(index);
            }
            else
            {
                _gameModel.MoveGpu(_slots.IndexOf(slotView));
            }
        }
        
        private void OnAnySlotHoverChanged(GpuSlotView slotView, bool isHovered)
        {
            var oldHoverGpu = HoverGpu;
            
            if (isHovered)
                HoverGpu = _slots.IndexOf(slotView);

            if (HoverGpu != null && !isHovered && _slots[HoverGpu.Value] == slotView)
                HoverGpu = null;
            
            if (oldHoverGpu != HoverGpu)
                HoverGpuChangedEvent?.Invoke(HoverGpu);
        }
    }
}