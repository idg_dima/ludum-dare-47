using System;
using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class WheelsTableView : MonoBehaviour
    {
        public event Action<int?> HoverWheelChangedEvent;
        
        [SerializeField] private Transform TableContainer;
        
        [Inject] private IInstantiator _instantiator;
        [Inject] private GameDictionary _dictionary;
        [Inject] private GameModel _gameModel;

        public Transform LookPoint => _tableView.LookPoint;
        public int? HoverWheel => _tableView.HoverWheel;
        
        private TableView _tableView;
        private bool _inputEnabled;

        private void OnEnable()
        {
            SetTablePrefab(_gameModel.WheelTable);
            _gameModel.WheelTableChangedEvent += OnWheelTableChanged;
        }

        private void OnDisable()
        {
            _gameModel.WheelTableChangedEvent -= OnWheelTableChanged;
        }

        private void OnWheelTableChanged(WheelTableType tableType)
        {
            SetTablePrefab(tableType);
        }

        private void SetTablePrefab(WheelTableType tableType)
        {
            if (_tableView != null)
                Destroy(_tableView.gameObject);
            
            var tableInfo = _dictionary.GetTableInfo(tableType);
            var tableObject = _instantiator.InstantiatePrefab(tableInfo.Prefab, TableContainer);
            _tableView = tableObject.GetComponent<TableView>();
            _tableView.SetInputEnabled(_inputEnabled);
            _tableView.HoverWheelChangedEvent += OnHoverWheelChanged;
        }

        private void OnHoverWheelChanged(int? index)
        {
            HoverWheelChangedEvent?.Invoke(index);
        }

        public bool OnBackPressed()
        {
            return _tableView.OnBackPressed();
        }
        
        public void SetInputEnabled(bool value)
        {
            _inputEnabled = value;
            _tableView.SetInputEnabled(value);
        }
    }
}