using System.Linq;
using Game.Dic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class GpusUi : MonoBehaviour
    {
        [SerializeField] private PcContainerView _pcContainerView;
        [SerializeField] private GpuTooltip _newGpuTooltip;
        [SerializeField] private GpuTooltip _currentGpuTooltip;
        [SerializeField] private GameObject _newGpuContainer;
        [SerializeField] private Button _deleteNewGpuButton;
        [SerializeField] private TextMeshProUGUI _energyInfoText;

        [Inject] private GameModel _gameModel;

        private void Awake()
        {
            _deleteNewGpuButton.onClick.AddListener(OnDeleteNewGpuClicked);
        }

        private void OnEnable()
        {
            ShowGpuTooltip(null);
            UpdateNewGpuTooltip();
            UpdateEnergyInfoText();
            
            _gameModel.NewGpuChangedEvent += OnNewGpuChanged;
            _pcContainerView.HoverGpuChangedEvent += OnHoverGpuChanged;
            _gameModel.GpuModelChangedEvent += OnAnyGpuModelChanged;
            _gameModel.WheelModelChangedEvent += OnAnyWheelModelChanged;
        }

        private void OnDisable()
        {
            _gameModel.NewGpuChangedEvent -= OnNewGpuChanged;
            _pcContainerView.HoverGpuChangedEvent -= OnHoverGpuChanged;
            _gameModel.GpuModelChangedEvent -= OnAnyGpuModelChanged;
            _gameModel.WheelModelChangedEvent -= OnAnyWheelModelChanged;
        }

        private void OnHoverGpuChanged(int? index)
        {
            if (index != null)
            {
                var gpuModel = _gameModel.GpuModels[index.Value];
                ShowGpuTooltip(gpuModel);
            }
            else
            {
                ShowGpuTooltip(null);
            }
        }
        
        private void OnAnyGpuModelChanged(int index)
        {
            if (_pcContainerView.HoverGpu == index)
            {
                var gpuModel = _gameModel.GpuModels[index];
                ShowGpuTooltip(gpuModel);
            }
            UpdateEnergyInfoText();
        }
        
        private void OnAnyWheelModelChanged(int index)
        {
            UpdateEnergyInfoText();
        }

        private void ShowGpuTooltip(GpuModel model)
        {
            _currentGpuTooltip.gameObject.SetActive(model != null);
            if (model != null)
            {
                if (model.GpuType == GpuType.None)
                    _currentGpuTooltip.SetEmpty();
                else
                    _currentGpuTooltip.SetGpuType(model.GpuType, _gameModel.GpuModels.IndexOf(model), model.IsActive);
            }
        }

        private void OnNewGpuChanged()
        {
            UpdateNewGpuTooltip();
        }

        private void UpdateNewGpuTooltip()
        {
            _newGpuContainer.SetActive(_gameModel.NewGpu != null);
            if (_gameModel.NewGpu != null)
                _newGpuTooltip.SetGpuType(_gameModel.NewGpu.Value);
        }

        private void OnDeleteNewGpuClicked()
        {
            _gameModel.DeleteNewGpu();
        }

        private void UpdateEnergyInfoText()
        {
            var totalRequirement = _gameModel.GpuModels.Sum(x => x.EnergyRequirement);
            _energyInfoText.text = $"Energy generatiion: {_gameModel.GetCurrentEnergy()}\nCombined GPUs requirements: {totalRequirement}";
        }
    }
}