using Game.Dic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Game
{
    public class HamsterTooltip : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _descriptionText;

        [Inject] private GameDictionary _dictionary;
        
        public void SetHamsterModel(HamsterModel hamster)
        {
            _nameText.text = "Hamster";

            var level = hamster.Level == HamsterModel.MaxLevel ? $"{hamster.Level} (max)" : hamster.Level.ToString();
            
            var text = $"Energy: {hamster.Energy}\nLevel: {level}\n Experience: {(int) hamster.Experience}/{hamster.NextLevel}";

            if (hamster.CoinsMultiplier > 0)
                text += $"\nCoins production +{hamster.CoinsMultiplier}%";
            
            if (hamster.OtherHamstersMultiplier > 0)
                text += $"\nOther hamsters Energy +{hamster.OtherHamstersMultiplier}%";
            
            if (hamster.ExperienceMultiplier > 0)
                text += $"\nOther hamsters XP +{hamster.ExperienceMultiplier}%";

            _descriptionText.text = text;
        }
    }
}