using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Game
{
    public class HamsterModel
    {
        public const int MaxLevel = 4;
        
        public event Action<HamsterModel, string> LevelUpEvent;
        
        private static readonly int[] ExperienceToLevel = {0, 150, 300, 500};
        
        public int Energy { get; private set; }
        public int OtherHamstersMultiplier { get; private set; }
        public int CoinsMultiplier { get; private set; }
        public int ExperienceMultiplier { get; private set; }

        public int Level { get; private set; }
        public float Experience { get; private set; }
        public int NextLevel { get; private set; }

        public HamsterModel(bool bad = false)
        {
            if (bad)
                Energy = 14;
            else
                Energy = Random.Range(10, 31);
            
            OtherHamstersMultiplier = 0;
            CoinsMultiplier = 0;
            ExperienceMultiplier = 0;

            Level = 1;
            Experience = 0;
            NextLevel = ExperienceToLevel[Level];
        }

        public void DoWork(float deltaTime, List<WheelModel> allWheelsModels)
        {
            if (Level == MaxLevel)
                return;

            var experienceMultipler = 0;
            foreach (var wheelModel in allWheelsModels)
            {
                if (wheelModel.HamsterModel != null && wheelModel.HamsterModel != this)
                    experienceMultipler += wheelModel.HamsterModel.ExperienceMultiplier;
            }
            
            Experience += deltaTime * (1 + experienceMultipler / 100f);
            if (Experience > NextLevel)
            {
                LevelUp();
            }
        }

        private void LevelUp()
        {
            Level = Level + 1;
            Experience = 0;
            NextLevel = ExperienceToLevel[Level];

            var message = "Your hamster just leveled up!";

            var hasSomething = false;
            
            if (Random.value < 0.25f)
            {
                var multiplier = Random.Range(1, 2) * 5;
                OtherHamstersMultiplier += multiplier;
                message += $"\nEnergy of other hamsters +{multiplier}%";
                hasSomething = true;
            }
            
            if (Random.value < 0.25f)
            {
                var multiplier = Random.Range(1, 2) * 5;
                CoinsMultiplier += multiplier;
                message += $"\nCoins production +{multiplier}%";
                hasSomething = true;
            }
            
            if (Random.value < 0.25f)
            {
                var multiplier = Random.Range(1, 2) * 5;
                ExperienceMultiplier += multiplier;
                message += $"\nExperience of other hamsters +{multiplier}%";
                hasSomething = true;
            }
            
            if (!hasSomething || Random.value < 0.2f)
            {
                Energy += 5;
                message += "\nEnergy +5";
            }
            
            LevelUpEvent?.Invoke(this, message);
        }
    }
}