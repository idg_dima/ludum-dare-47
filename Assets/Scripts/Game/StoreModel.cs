using System;
using System.Collections.Generic;
using Game.Dic;
using Zenject;

namespace Game
{
    public class StoreModel
    {
        public event Action<StorePageModel> PageChangedEvent;
        
        public readonly List<StorePageModel> Pages;

        private readonly GameDictionary _dictionary;
        private readonly GameModel _gameModel;
        private readonly StorePageModel _updagradesPage;
        private readonly StorePageModel _gpuPage;
        private readonly StorePageModel _wheelsPage;
        private readonly StorePageModel _hamstersPage;

        [Inject]
        public StoreModel(GameDictionary dictionary, GameModel gameModel)
        {
            _dictionary = dictionary;
            _gameModel = gameModel;
            
            _updagradesPage = new StorePageModel("Upgrades");
            _updagradesPage.Items = new List<StoreItemModel>
            {
                new StoreItemModel("Medium Table", "+1 wheel slot", 5, UpgradeTable1),
                new StoreItemModel("Bigger PC", "+1 gpu slot", 2, UpgradePc1),
            };
            
            _wheelsPage = new StorePageModel("Hamster wheels");
            _wheelsPage.Items = new List<StoreItemModel>
            {
                new StoreItemModel(_dictionary.Wheels[0].Name, _dictionary.Wheels[0].Description, 5, () => _gameModel.GiveWheel(WheelType.Tier1)),
                new StoreItemModel(_dictionary.Wheels[1].Name, _dictionary.Wheels[1].Description, 30, () => _gameModel.GiveWheel(WheelType.Tier2)),
                new StoreItemModel(_dictionary.Wheels[2].Name, _dictionary.Wheels[2].Description, 75, () => _gameModel.GiveWheel(WheelType.Tier3)),
            };
            
            _gpuPage = new StorePageModel("GPU");
            _gpuPage.Items = new List<StoreItemModel>
            {
                new StoreItemModel(_dictionary.Gpus[0].Name, _dictionary.Gpus[0].Description, 5, () => _gameModel.GiveGpu(GpuType.Tier1)),
                new StoreItemModel(_dictionary.Gpus[1].Name, _dictionary.Gpus[1].Description, 20, () => _gameModel.GiveGpu(GpuType.Tier2)),
                new StoreItemModel(_dictionary.Gpus[2].Name, _dictionary.Gpus[2].Description, 50, () => _gameModel.GiveGpu(GpuType.Tier3)),
            };
            
            _hamstersPage = new StorePageModel("Hamsters");
            _hamstersPage.Items = new List<StoreItemModel>
            {
                new StoreItemModel("New hamster", "Random new hamster for your dirty needs", 10, () => _gameModel.GiveHamster()),
            };
            
            Pages = new List<StorePageModel>
            {
                _updagradesPage,
                _wheelsPage,
                _gpuPage,
                _hamstersPage,
            };
        }

        private void UpgradeTable1()
        {
            _gameModel.SetTableType(WheelTableType.Tier2);
            _updagradesPage.Items[0] = new StoreItemModel("Large Table", "+1 wheel slot", 100, () => _gameModel.SetTableType(WheelTableType.Tier3), 1);
            PageChangedEvent?.Invoke(_updagradesPage);
        }
        
        private void UpgradePc1()
        {
            _gameModel.SetPcType(PcType.Tier2);
            _updagradesPage.Items[1] = new StoreItemModel("Biggest PC", "+2 gpu slot", 50, () => _gameModel.SetPcType(PcType.Tier3), 1);
            PageChangedEvent?.Invoke(_updagradesPage);
        }
    }
}