using System.Collections.Generic;
using Generated;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Game
{
    
    public class CameraClickDetector : MonoBehaviour
    {
        [SerializeField] private Camera _camera;

        [Inject] private InputActions _inputActions;

        private RaycastHit[] _hits = new RaycastHit[10];
        private HashSet<GameObject> _hoveredGameObjects = new HashSet<GameObject>();
        private HashSet<GameObject> _newHoveredGameObjects = new HashSet<GameObject>();

        private void OnEnable()
        {
            _inputActions.Default.MouseClick.performed += OnMouseClickPerformed;
            _inputActions.Default.MouseRight.performed += OnMouseRightPerformed;
        }
        
        private void OnDisable()
        {
            _inputActions.Default.MouseClick.performed -= OnMouseClickPerformed;
            _inputActions.Default.MouseRight.performed -= OnMouseRightPerformed;
        }

        private void Update()
        {
            var ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());
            var count = Physics.RaycastNonAlloc(ray, _hits);

            for (var i = 0; i < count; i++)
            {
                _newHoveredGameObjects.Add(_hits[i].collider.gameObject);
            }

            foreach (var oldHovered in _hoveredGameObjects)
            {
                if (!_newHoveredGameObjects.Contains(oldHovered))
                {
                    var clickableCollider = oldHovered.GetComponent<ClickableCollider>();
                    if (clickableCollider != null)
                        clickableCollider.SetIsHovered(false);
                }
            }
            
            foreach (var newHovered in _newHoveredGameObjects)
            {
                if (!_hoveredGameObjects.Contains(newHovered))
                {
                    var clickableCollider = newHovered.GetComponent<ClickableCollider>();
                    if (clickableCollider != null)
                        clickableCollider.SetIsHovered(true);
                }
            }

            (_hoveredGameObjects, _newHoveredGameObjects) = (_newHoveredGameObjects, _hoveredGameObjects);
            _newHoveredGameObjects.Clear();
        }

        private void OnMouseClickPerformed(InputAction.CallbackContext context)
        {
            var ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

            var hits = Physics.RaycastAll(ray);

            foreach (var hit in hits)
            {
                var clickableCollider = hit.collider.GetComponent<ClickableCollider>();
                if (clickableCollider != null)
                    clickableCollider.Click();
            }
        }
        
        private void OnMouseRightPerformed(InputAction.CallbackContext context)
        {
            var ray = _camera.ScreenPointToRay(Mouse.current.position.ReadValue());

            var hits = Physics.RaycastAll(ray);

            foreach (var hit in hits)
            {
                var clickableCollider = hit.collider.GetComponent<ClickableCollider>();
                if (clickableCollider != null)
                    clickableCollider.RightClick();
            }
        }
    }
}