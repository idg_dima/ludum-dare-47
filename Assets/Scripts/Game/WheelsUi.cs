using System;
using Game.Dic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class WheelsUi : MonoBehaviour
    {
        [SerializeField] private WheelsTableView _wheelsTable;
        [SerializeField] private WheelTooltip _newWheelTooltip;
        [SerializeField] private HamsterTooltip _newHamsterTooltip;
        [SerializeField] private WheelTooltip _currentWheelTooltip;
        [SerializeField] private HamsterTooltip _currentHamsterTooltip;
        [SerializeField] private GameObject _newWheelContainer;
        [SerializeField] private GameObject _newHamsterContainer;
        [SerializeField] private Button _deleteNewHamsterButton;

        [Inject] private GameModel _gameModel;

        private void Awake()
        {
            _deleteNewHamsterButton.onClick.AddListener(OnDeleteNewHamsterClicked);
        }

        private void OnEnable()
        {
            ShowWheelTooltip(WheelType.None);
            ShowHamsterTooltip(null);
            UpdateNewWheelTooltip();
            UpdateNewHamsterTooltip();
            
            _gameModel.NewWheelChangedEvent += OnNewWheelChanged;
            _gameModel.NewHamsterChangedEvent += OnNewHamsterChanged;
            _wheelsTable.HoverWheelChangedEvent += OnHoverWheelChanged;
            _gameModel.WheelModelChangedEvent += OnAnyWheelModelChanged;
        }

        private void OnDisable()
        {
            _gameModel.NewWheelChangedEvent -= OnNewWheelChanged;
            _gameModel.NewHamsterChangedEvent -= OnNewHamsterChanged;
            _wheelsTable.HoverWheelChangedEvent -= OnHoverWheelChanged;
            _gameModel.WheelModelChangedEvent -= OnAnyWheelModelChanged;
        }

        private void OnHoverWheelChanged(int? index)
        {
            if (index != null)
            {
                var wheelModel = _gameModel.WheelModels[index.Value];
                ShowWheelTooltip(wheelModel.WheelType);
                ShowHamsterTooltip(wheelModel.HamsterModel);
            }
            else
            {
                ShowWheelTooltip(null);
                ShowHamsterTooltip(null);
            }
        }
        
        private void OnAnyWheelModelChanged(int index)
        {
            if (_wheelsTable.HoverWheel == index)
            {
                var wheelModel = _gameModel.WheelModels[index];
                ShowWheelTooltip(wheelModel.WheelType);
                ShowHamsterTooltip(wheelModel.HamsterModel);
            }
        }

        private void ShowWheelTooltip(WheelType? type)
        {
            _currentWheelTooltip.gameObject.SetActive(type != null);
            if (type != null)
            {
                if (type == WheelType.None)
                    _currentWheelTooltip.SetEmpty();
                else
                    _currentWheelTooltip.SetWheelType(type.Value);
            }
        }
        
        private void ShowHamsterTooltip(HamsterModel hamsterModel)
        {
            _currentHamsterTooltip.gameObject.SetActive(hamsterModel != null);
            if (hamsterModel != null)
                _currentHamsterTooltip.SetHamsterModel(hamsterModel);
        }

        private void OnNewWheelChanged()
        {
            UpdateNewWheelTooltip();
        }

        private void OnNewHamsterChanged()
        {
            UpdateNewHamsterTooltip();
        }

        private void UpdateNewWheelTooltip()
        {
            _newWheelContainer.SetActive(_gameModel.NewWheel != null);
            if (_gameModel.NewWheel != null)
                _newWheelTooltip.SetWheelType(_gameModel.NewWheel.Value);
        }

        private void UpdateNewHamsterTooltip()
        {
            _newHamsterContainer.SetActive(_gameModel.NewHamster != null);
            if (_gameModel.NewHamster != null)
                _newHamsterTooltip.SetHamsterModel(_gameModel.NewHamster);
        }

        private void OnDeleteNewHamsterClicked()
        {
            _gameModel.DeleteNewHamster();
        }
    }
}