using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game
{
    public class ComputerScreenView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _balanceText;
        [SerializeField] private Image _progressImage;
        [SerializeField] private Button _shopButton;
        [SerializeField] private GameObject _mainView;
        [SerializeField] private StoreView _storeView;

        [Inject] private GameModel _gameModel;

        private void Awake()
        {
            _shopButton.onClick.AddListener(OnShopButtonClicked);
            _storeView.BackClickedAction += OnStoreClosed;
            
            SetStoreActive(false);
        }

        private void OnEnable()
        {
            _gameModel.BalanceChangedEvent += OnBalanceChanged;
            _balanceText.text = _gameModel.Balance.ToString();
        }

        private void OnDisable()
        {
            _gameModel.BalanceChangedEvent -= OnBalanceChanged;
        }

        private void OnBalanceChanged(int balance)
        {
            _balanceText.text = balance.ToString();
        }

        private void Update()
        {
            _progressImage.fillAmount = _gameModel.CoinProgress;
        }

        private void OnShopButtonClicked()
        {
            SetStoreActive(true);
        }

        private void OnStoreClosed()
        {
            SetStoreActive(false);
        }

        private void SetStoreActive(bool value)
        {
            _mainView.SetActive(!value);
            _storeView.gameObject.SetActive(value);
        }
        
        public void SetInputEnabled(bool value)
        {
            _shopButton.interactable = value;
            if (!value)
                SetStoreActive(false);
        }
    }
}