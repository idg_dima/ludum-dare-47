using System;
using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class PcContainerView : MonoBehaviour
    {
        public event Action<int?> HoverGpuChangedEvent;
        
        [SerializeField] private Transform PcContainer;
        
        [Inject] private IInstantiator _instantiator;
        [Inject] private GameDictionary _dictionary;
        [Inject] private GameModel _gameModel;

        public int? HoverGpu => _pcView.HoverGpu;
        public Transform LookPoint => _pcView.LookPoint;
        
        private PcView _pcView;
        private bool _inputEnabled;

        private void OnEnable()
        {
            SetPcPrefab(_gameModel.Pc);
            _gameModel.PcChangedEvent += OnPcChanged;
        }

        private void OnDisable()
        {
            _gameModel.PcChangedEvent -= OnPcChanged;
        }

        private void OnPcChanged(PcType pcType)
        {
            SetPcPrefab(pcType);
        }

        private void SetPcPrefab(PcType pcType)
        {
            if (_pcView != null)
                Destroy(_pcView.gameObject);
            
            var pcInfo = _dictionary.GetPcInfo(pcType);
            var pcObject = _instantiator.InstantiatePrefab(pcInfo.Prefab, PcContainer);
            _pcView = pcObject.GetComponent<PcView>();
            _pcView.SetInputEnabled(_inputEnabled);
            _pcView.HoverGpuChangedEvent += OnHoverGpuChanged;
        }

        public void SetInputEnabled(bool value)
        {
            _inputEnabled = value;
            _pcView.SetInputEnabled(value);
        }
        
        
        private void OnHoverGpuChanged(int? index)
        {
            HoverGpuChangedEvent?.Invoke(index);
        }
    }
}