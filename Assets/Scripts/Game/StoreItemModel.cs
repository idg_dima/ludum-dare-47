using System;

namespace Game
{
    public class StoreItemModel
    {
        public event Action PurchasedEvent;
        
        public string Name;
        public string Description;
        public int Price;
        public Action RewardAction;
        public int? Limit;

        public bool IsSoldOut => Limit != null && Limit <= 0;

        public StoreItemModel(string name, string description, int price, Action rewardAction, int? limit = null)
        {
            Name = name;
            Description = description;
            Price = price;
            RewardAction = rewardAction;
            Limit = limit;
        }

        public void Purchase()
        {
            if (Limit != null)
            {
                Limit--;
            }
            RewardAction?.Invoke();
            PurchasedEvent?.Invoke();
        }
    }
}