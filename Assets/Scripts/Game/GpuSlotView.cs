using System;
using Game.Dic;
using UnityEngine;
using Zenject;

namespace Game
{
    public class GpuSlotView : MonoBehaviour
    {
        public Action<GpuSlotView> ClickedEvent;
        public Action<GpuSlotView, bool> HoverChangedEvent;
        
        [SerializeField] private ClickableCollider _collider;
        [SerializeField] private Transform _container;
        [Inject] private IInstantiator _instantiator;
        [Inject] private GameDictionary _dictionary;

        private GameObject _gpuView;
        private GpuType _gpuType;
        private GameObject _wheel;
     
        private void Awake()
        {
            _collider.ClickedEvent += () => ClickedEvent?.Invoke(this);
            _collider.HoveredChangedEvent += x => HoverChangedEvent?.Invoke(this, x);
        }
        
        public void UpdateModel(GpuModel model)
        {
            if (_gpuType != model.GpuType)
                SetGpuType(model.GpuType);
        }

        private void SetGpuType(GpuType gpuType)
        {
            if (_gpuView != null)
            {
                Destroy(_gpuView);
                _gpuView = null;
            }

            _gpuType = gpuType;
            if (_gpuType != GpuType.None)
            {
                var gpuInfo = _dictionary.GetGpuInfo(_gpuType);
                _gpuView = _instantiator.InstantiatePrefab(gpuInfo.Prefab, _container);
            }
        }

        public void SetInputEnabled(bool value)
        {
            _collider.gameObject.SetActive(value);
        }
    }
}