using System.Data;
using Game.Dic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Game
{
    public class GpuTooltip : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private TextMeshProUGUI _descriptionText;
        [SerializeField] private TextMeshProUGUI _slotText;
        [SerializeField] private TextMeshProUGUI _statusText;
        [SerializeField] private GameObject _statusContainer;
        [SerializeField] private Color _statusActiveColor;
        [SerializeField] private Color _statusNotActiveColor;

        [Inject] private GameDictionary _dictionary;
        [Inject] private GameModel _gameModel;
        
        public void SetGpuType(GpuType type)
        {
            ShowData(type);
            _statusContainer.SetActive(false);
        }

        private void ShowData(GpuType type)
        {
            var gpuInfo = _dictionary.GetGpuInfo(type);
            _nameText.text = gpuInfo.Name;
            _descriptionText.text = gpuInfo.Description;
        }
        
        public void SetGpuType(GpuType type, int slotIndex, bool isActive)
        {
            ShowData(type);
            _statusContainer.SetActive(true);

            _slotText.text = $"Slot {slotIndex + 1}/{_gameModel.GpuModels.Count}";
            _statusText.text = isActive ? "Active" : "Not enough energy";
            _statusText.color = isActive ? _statusActiveColor : _statusNotActiveColor;
        }

        public void SetEmpty()
        {
            _nameText.text = "Empty slot";
            _descriptionText.text = "Purchase GPU in store and place it here";
            _statusContainer.SetActive(false);
        }
    }
}