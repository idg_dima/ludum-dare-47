// GENERATED AUTOMATICALLY FROM 'Assets/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Generated
{
    public class @InputActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @InputActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""Test"",
            ""id"": ""96c3bf38-5d83-4e9f-ae4a-d5a61b9f17c5"",
            ""actions"": [
                {
                    ""name"": ""Test"",
                    ""type"": ""Button"",
                    ""id"": ""c6a80739-9c1c-49b0-b69e-a983da3030a5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2a67fd0f-b948-445a-988d-3547443c67a5"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Test"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Default"",
            ""id"": ""6d6fc8d0-168e-4baa-8108-2de744e937a7"",
            ""actions"": [
                {
                    ""name"": ""Mouse Click"",
                    ""type"": ""Button"",
                    ""id"": ""d8051044-9bef-4a78-9996-a8f557f2d41a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Mouse Right"",
                    ""type"": ""Button"",
                    ""id"": ""ef836db7-0a6b-4219-921f-cef68b801fcf"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""dbc227e4-9f78-42d9-9ef6-d50ca1bf0633"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a95ae3a2-a342-45cb-8f30-9ca8887ca834"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Mouse Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""caecf3f8-f54a-485e-9ea3-8b1f8c9e307c"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2c7539c1-429a-4f07-87aa-3a3f6daf14a2"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Mouse Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Test
            m_Test = asset.FindActionMap("Test", throwIfNotFound: true);
            m_Test_Test = m_Test.FindAction("Test", throwIfNotFound: true);
            // Default
            m_Default = asset.FindActionMap("Default", throwIfNotFound: true);
            m_Default_MouseClick = m_Default.FindAction("Mouse Click", throwIfNotFound: true);
            m_Default_MouseRight = m_Default.FindAction("Mouse Right", throwIfNotFound: true);
            m_Default_Back = m_Default.FindAction("Back", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Test
        private readonly InputActionMap m_Test;
        private ITestActions m_TestActionsCallbackInterface;
        private readonly InputAction m_Test_Test;
        public struct TestActions
        {
            private @InputActions m_Wrapper;
            public TestActions(@InputActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Test => m_Wrapper.m_Test_Test;
            public InputActionMap Get() { return m_Wrapper.m_Test; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(TestActions set) { return set.Get(); }
            public void SetCallbacks(ITestActions instance)
            {
                if (m_Wrapper.m_TestActionsCallbackInterface != null)
                {
                    @Test.started -= m_Wrapper.m_TestActionsCallbackInterface.OnTest;
                    @Test.performed -= m_Wrapper.m_TestActionsCallbackInterface.OnTest;
                    @Test.canceled -= m_Wrapper.m_TestActionsCallbackInterface.OnTest;
                }
                m_Wrapper.m_TestActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Test.started += instance.OnTest;
                    @Test.performed += instance.OnTest;
                    @Test.canceled += instance.OnTest;
                }
            }
        }
        public TestActions @Test => new TestActions(this);

        // Default
        private readonly InputActionMap m_Default;
        private IDefaultActions m_DefaultActionsCallbackInterface;
        private readonly InputAction m_Default_MouseClick;
        private readonly InputAction m_Default_MouseRight;
        private readonly InputAction m_Default_Back;
        public struct DefaultActions
        {
            private @InputActions m_Wrapper;
            public DefaultActions(@InputActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @MouseClick => m_Wrapper.m_Default_MouseClick;
            public InputAction @MouseRight => m_Wrapper.m_Default_MouseRight;
            public InputAction @Back => m_Wrapper.m_Default_Back;
            public InputActionMap Get() { return m_Wrapper.m_Default; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(DefaultActions set) { return set.Get(); }
            public void SetCallbacks(IDefaultActions instance)
            {
                if (m_Wrapper.m_DefaultActionsCallbackInterface != null)
                {
                    @MouseClick.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseClick;
                    @MouseClick.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseClick;
                    @MouseClick.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseClick;
                    @MouseRight.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseRight;
                    @MouseRight.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseRight;
                    @MouseRight.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnMouseRight;
                    @Back.started -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBack;
                    @Back.performed -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBack;
                    @Back.canceled -= m_Wrapper.m_DefaultActionsCallbackInterface.OnBack;
                }
                m_Wrapper.m_DefaultActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @MouseClick.started += instance.OnMouseClick;
                    @MouseClick.performed += instance.OnMouseClick;
                    @MouseClick.canceled += instance.OnMouseClick;
                    @MouseRight.started += instance.OnMouseRight;
                    @MouseRight.performed += instance.OnMouseRight;
                    @MouseRight.canceled += instance.OnMouseRight;
                    @Back.started += instance.OnBack;
                    @Back.performed += instance.OnBack;
                    @Back.canceled += instance.OnBack;
                }
            }
        }
        public DefaultActions @Default => new DefaultActions(this);
        private int m_KeyboardSchemeIndex = -1;
        public InputControlScheme KeyboardScheme
        {
            get
            {
                if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
                return asset.controlSchemes[m_KeyboardSchemeIndex];
            }
        }
        public interface ITestActions
        {
            void OnTest(InputAction.CallbackContext context);
        }
        public interface IDefaultActions
        {
            void OnMouseClick(InputAction.CallbackContext context);
            void OnMouseRight(InputAction.CallbackContext context);
            void OnBack(InputAction.CallbackContext context);
        }
    }
}
