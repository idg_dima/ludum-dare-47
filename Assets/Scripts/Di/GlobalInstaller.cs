using Game;
using Game.Dic;
using Generated;
using UnityEngine;
using Zenject;

namespace Di
{
    public class GlobalInstaller : MonoInstaller
    {
        [SerializeField] private GameDictionary _gameDictionary;
        
        public override void InstallBindings()
        {
            var inputActions = new InputActions();
            inputActions.Enable();
            Container.BindInstance(inputActions);

            Container.Bind<GameModel>().AsSingle().NonLazy();
            Container.Bind<StoreModel>().AsSingle().NonLazy();

            Container.Bind<LifecycleManager>().FromNewComponentOnNewGameObject().AsSingle();
            
            Container.BindInstance(_gameDictionary);
        }
    }
}